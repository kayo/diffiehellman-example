DiffieHellman Usage Examples
============================

Haskell Example
---------------

Source: *Main.hs*

Compile and run with Cabal:

```

cabal sandbox init
cabal update
cabal install --dependencies-only
cabal configure
cabal build
cabal run

```

Using AES RNG:

```

cabal sandbox init
cabal update
cabal install --dependencies-only -f aesrng
cabal configure -f aesrng
cabal build
cabal run

```

CoffeeScript Example (NodeJS/Browserify)
----------------------------------------

Source: *Main.coffee*

Run NodeJS example using npm:

```

npm run example

```

Run with coffee:

```

coffee Main.coffee

```

Compile debug version with npm and run using browser:

```

npm debug
<your-browser> index.html # see console output

```

Compile minified version with npm and run in browser:

```

npm release
<your-browser> index.min.html # see console output

```

Have fun!
