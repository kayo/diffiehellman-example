-- Haskell Diffie-Hellman example
import Crypto.Random( createEntropyPool
                    , cprgCreate )
#ifdef USE_AESRNG
import Crypto.Random.AESCtr( AESRNG )
#else
import Crypto.Random( SystemRNG )
#endif
import Crypto.PubKey.DH
import Prelude
import Numeric( showHex )

#ifdef USE_AESRNG
type UsedRNG = AESRNG
#else
type UsedRNG = SystemRNG
#endif

main = do
  let bits = 256
      generator = 2
  
  serverEntropyPool <- createEntropyPool
  
  -- the prime is shared by everyone
  let serverG = cprgCreate serverEntropyPool :: UsedRNG
      ((Params prime _), _) = generateParams serverG bits generator

  putStrLn $ "prime: " ++ showHex prime ""

  -- sharing secret key on a pair
  aliceEntropyPool <- createEntropyPool

  let aliceG = cprgCreate aliceEntropyPool :: UsedRNG
      aliceParams = Params prime generator
      (alicePriv, _) = generatePrivate aliceG aliceParams
      alicePub = calculatePublic aliceParams alicePriv

  bobEntropyPool <- createEntropyPool

  let bobG = cprgCreate bobEntropyPool :: UsedRNG
      bobParams = Params prime generator
      (bobPriv, _) = generatePrivate bobG bobParams
      bobPub = calculatePublic bobParams bobPriv

  putStrLn $ "Key exchange: Alice <-> Bob"

  let bobAliceSecret = getShared bobParams bobPriv alicePub
      aliceBobSecret = getShared aliceParams alicePriv bobPub

  -- public keys are different, but secret is common
  putStrLn $ "Public keys are different? " ++ if alicePub /= bobPub then pass else fail
  putStrLn $ "Secret keys is common? " ++ if bobAliceSecret == aliceBobSecret then pass else fail
  
  -- use common secret as shared encryption key...
  
  carolEntropyPool <- createEntropyPool
  
  -- shared secret with 3rd person
  let carolG = cprgCreate carolEntropyPool :: UsedRNG
      carolParams = Params prime generator
      (carolPriv, _) = generatePrivate carolG carolParams
      carolPub = calculatePublic carolParams carolPriv

  putStrLn $ "Key exchange: Alice <-> Carol"

  let carolAliceSecret = getShared carolParams carolPriv alicePub
      aliceCarolSecret = getShared aliceParams alicePriv carolPub

  -- public keys are different, but secret is common
  putStrLn $ "Public keys are different? " ++ if alicePub /= carolPub then pass else fail
  putStrLn $ "Secret keys is common? " ++ if carolAliceSecret == aliceCarolSecret then pass else fail
  
  -- secret depends on pairs
  putStrLn $ "Secret depends on pairs? " ++ if aliceBobSecret /= aliceCarolSecret then pass else fail
  
  putStrLn $ "Key exchange: Carol <- Bob"
  
  let carolBobSecret = getShared carolParams carolPriv bobPub

  putStrLn $ "Secret depends on pairs? " ++ if carolAliceSecret /= carolBobSecret then pass else fail

  return ()

  where
    pass = "Yes!"
    fail = "No... Something is wrong!"
