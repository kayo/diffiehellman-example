# Node.JS Diffie-Hellman example
{ createDiffieHellman } = require "crypto"
log = (args...)-> console.log args...

bits = 256
# generator = 2 -- node's default

pass = "Yes!"
fail = "No... Something is wrong!"

# the prime is shared by everyone
server = createDiffieHellman bits
prime = do server.getPrime

log "prime: " + (prime.toString "hex")

# sharing secret key on a pair
alice = createDiffieHellman prime
alicePub = do alice.generateKeys

bob = createDiffieHellman prime
bobPub = do bob.generateKeys

log "Key exchange: Alice <-> Bob"

bobAliceSecret = bob.computeSecret alicePub
aliceBobSecret = alice.computeSecret bobPub

# public keys are different, but secret is common
log "Public keys are different? " + if (alicePub.toString 'hex') != (bobPub.toString 'hex') then pass else fail
log "Secret keys is common? " + if (bobAliceSecret.toString 'hex') == (aliceBobSecret.toString 'hex') then pass else fail

# use common secret as shared encryption key...

# shared secret with 3rd person
carol = createDiffieHellman prime
carolPub = do carol.generateKeys

log "Key exchange: Alice <-> Carol"

carolAliceSecret = carol.computeSecret alicePub
aliceCarolSecret = alice.computeSecret carolPub

# public keys are different, but secret is common
log "Public keys are different? " + if (alicePub.toString 'hex') != (carolPub.toString 'hex') then pass else fail
log "Secret keys is common? " + if (carolAliceSecret.toString 'hex') == (aliceCarolSecret.toString 'hex') then pass else fail

# secret depends on pairs
log "Secret depends on pairs? " + if (aliceBobSecret.toString 'hex') != (aliceCarolSecret.toString 'hex') then pass else fail

log "Key exchange: Carol <- Bob"

carolBobSecret = carol.computeSecret bobPub

log "Secret depends on pairs? " + if (carolAliceSecret.toString 'hex') != (carolBobSecret.toString 'hex') then pass else fail
